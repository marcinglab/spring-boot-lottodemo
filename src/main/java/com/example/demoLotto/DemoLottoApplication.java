package com.example.demoLotto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoLottoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoLottoApplication.class, args);
	}
}
